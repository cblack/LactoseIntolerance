QtApplication {
    cpp.includePaths: ["src"]
    files: [
        "src/*.cpp",
        "src/*.h",
    ]
    Group {
        files: ["qrc/**"]
        fileTags: "qt.core.resource_data"
        Qt.core.resourceSourceBase: "qrc/"
        Qt.core.resourcePrefix: "/"
    }
    Depends { name: "Qt"; submodules: ["qml", "quick", "quickcontrols2", "widgets"] }
}
