#pragma once

#include <QObject>
#include <QQuickItem>

class Helper : public QObject
{

    Q_OBJECT

public:

    Q_INVOKABLE void stackBefore(QQuickItem* one, QQuickItem* two) {
        one->stackBefore(two);
    }

};
