#include <QtCore>

#ifdef Q_OS_LINUX

#include <QApplication>
#define AppType QApplication

#else

#include <QGuiApplication>
#define AppType QGuiApplication

#endif

#include <QQmlApplicationEngine>

#include "helper.h"

int main(int argc, char *argv[])
{
    AppType app(argc, argv);
    app.setOrganizationName("KDE");
    app.setOrganizationDomain("kde.org");
    app.setApplicationName("LactoseIntolerance");

    qmlRegisterSingletonType<Helper>("org.kde.LactoseIntolerance", 1, 0, "Helper", [](QQmlEngine*,QJSEngine*) -> QObject* { return new Helper; });

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/Main.qml"));
    QObject::connect(
        &engine,
        &QQmlApplicationEngine::objectCreated,
        &app,
        [url](QObject *obj, const QUrl &objUrl) {
            if ((obj == nullptr) && url == objUrl) {
                QCoreApplication::exit(-1);
            }
        },
        Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
