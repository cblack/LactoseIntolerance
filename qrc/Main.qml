import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Layouts 1.10
import QtQuick.Controls 2.10 as QQC2
import org.kde.kirigami 2.10 as Kirigami
import Qt.labs.settings 1.0
import org.kde.LactoseIntolerance 1.0

Window {
    visible: true
    height: 200
    width: 200

    color: Kirigami.Theme.backgroundColor

    Component {
        id: mu

        QQC2.Button { }
    }

    Flow {
        id: flow

        width: parent.width
        height: parent.height

        QQC2.TextField {
            color: Kirigami.Theme.textColor
            selectedTextColor: Kirigami.Theme.highlightedTextColor
            selectionColor: Kirigami.Theme.highlightColor

            width: Math.max(leftPadding + contentWidth + rightPadding, 20)

            Keys.onSpacePressed: {
                let it = mu.createObject(flow, {text: this.text})
                Helper.stackBefore(it, this)
                this.text = ""
            }
            Keys.onPressed: (event) => {
                if (event.key == Qt.Key_Backspace && this.text == "") {
                    this.text = flow.data[Array.from(flow.data).length-2].text
                    flow.data[Array.from(flow.data).length-2].destroy()
                }
            }
        }
    }
}
